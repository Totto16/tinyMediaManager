package org.tinymediamanager.core.entities;

import java.util.Date;

import org.tinymediamanager.core.tvshow.TvShowModuleManager;
import org.tinymediamanager.core.tvshow.entities.TvShowEpisode;

public enum DummyType {

    NOT_A_DUMMY,
    NORMAL,
    SPECIAL,
    NOT_AIRED_YET,
    NO_AIR_DATE;

    public boolean isEnabled() {
        switch (this) {
            case NORMAL:
                return TvShowModuleManager.getInstance().getSettings().isDisplayMissingEpisodes();

            case SPECIAL:
                return TvShowModuleManager.getInstance().getSettings().isDisplayMissingSpecials();

            case NOT_AIRED_YET:
                return TvShowModuleManager.getInstance().getSettings().isDisplayMissingNotAired();

            default:
                return false;
        }
    }

    public static DummyType getDummyType(TvShowEpisode episode) {
        if (!episode.isDummy()) {
            return NOT_A_DUMMY;
        }

        if (episode.getFirstAired() == null) {
            return NO_AIR_DATE;
        }

        if (episode.getFirstAired().compareTo(new Date()) > 0) {
            return NOT_AIRED_YET;
        }

        if (episode.getSeason() == 0) {
            return SPECIAL;
        }

        return NORMAL;
    }
}